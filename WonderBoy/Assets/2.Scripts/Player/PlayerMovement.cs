﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D rb;
    private BoxCollider2D boxCollider;
    private SpriteRenderer sr;
    private float refVelocity;

    public float moveDir;
    public float moveSpeed = 4500f;
    public float maxSpeed = 5.5f;
    public float jumpPower = 17f;

    public float slideRate = 0.35f;
    public float attackSlideRate = 0.25f;

    public LayerMask whatisGround;
    public Animator anim;

    public bool isGround;


    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        boxCollider = GetComponent<BoxCollider2D>();
        sr = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        PlayerInput();
        GroundCheck();
        PlayerAnim();
        GroundFriction();
    }

    private void FixedUpdate()
    {
        if (!IsPlayingAnim("Attack"))
        {
            if(PlayerFlip() || Mathf.Abs(moveDir * rb.velocity.x) < maxSpeed)
            {
                rb.AddForce(new Vector2(moveDir * Time.fixedDeltaTime * moveSpeed, 0));
            }
            else
            {
                rb.velocity = new Vector2(moveDir * maxSpeed, rb.velocity.y);
            }
        }
        //else
        //{
        //    rb.velocity = new Vector2(moveDir * maxSpeed, rb.velocity.y);
        //}
    }

    private void PlayerInput()
    {
        moveDir = Input.GetAxisRaw("Horizontal");

        if(Input.GetKeyDown(KeyCode.Space) && isGround && !IsPlayingAnim("Attack"))
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpPower);
            MyAnimSetTrigger("Jump");
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            MyAnimSetTrigger("Attack");
        }
    }

    private bool PlayerFlip()
    {
        bool flipSprite = (sr.flipX ? (moveDir > 0f) : (moveDir < 0f));
        if (flipSprite)
        {
            sr.flipX = !sr.flipX;
            GroundFriction();
        }
        return flipSprite;
    }

    private void GroundCheck()
    {
        if(Physics2D.BoxCast(boxCollider.bounds.center, boxCollider.size, 0f, Vector2.down, 0.01f, whatisGround))
        {
            isGround = true;
            anim.ResetTrigger("Idle");
            //Debug.Log("isGround = true");
        }
        else
        {
            isGround = false;
            //Debug.Log("isGround = false");
        }
    }

    private void PlayerAnim()
    {
        if (isGround && !IsPlayingAnim("Attack"))
        {
            if((Mathf.Abs(moveDir) <= 0.01f || Mathf.Abs(rb.velocity.x) <= 0.01f && Mathf.Abs(rb.velocity.y) <= 0.01f))
            {
                MyAnimSetTrigger("Idle");
            }
            else if(Mathf.Abs(rb.velocity.x) > 0.01f && Mathf.Abs(rb.velocity.y) <= 0.01f)
            {
                MyAnimSetTrigger("Walk");
            }
        }
        //else if(rb.velocity.y < 0 && !IsPlayingAnim("Jump"))
        //{
        //    MyAnimSetTrigger("Down");
        //}
    }

    private void GroundFriction()
    {
        if (isGround)
        {
            if (IsPlayingAnim("Attack"))
            {
                rb.velocity = new Vector2(Mathf.SmoothDamp(rb.velocity.x, 0f, ref refVelocity, slideRate + attackSlideRate), rb.velocity.y);
            }
            else if(Mathf.Abs(moveDir) <= 0.01f)
            {
                rb.velocity = new Vector2(Mathf.SmoothDamp(rb.velocity.x, 0f, ref refVelocity, slideRate), rb.velocity.y);
            }
        }
    }

    private bool IsPlayingAnim(string animName)
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName(animName))
        {
            return true;
        }
        return false;
    }

    private void MyAnimSetTrigger(string animName)
    {
        if (!IsPlayingAnim(animName))
        {
            anim.SetTrigger(animName);
        }
    }
}
